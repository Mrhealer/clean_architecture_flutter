import 'package:apis_app/features/apis/domain/entities/waitingList.dart';
import 'package:json_annotation/json_annotation.dart';

/// statusCode : 200
/// message : null

@JsonSerializable()
class WaitingListModels extends WaitingListEntities {
  List<Data> data;
  int statusCode;
  Object message;

  WaitingListModels({this.data, this.statusCode, this.message});

  WaitingListModels.fromJson(Map<String, dynamic> json) {
    if (json['data'] != null) {
      data = new List<Data>();
      json['data'].forEach((v) {
        data.add(new Data.fromJson(v));
      });
    }
    statusCode = json['statusCode'];
    message = json['message'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    data['statusCode'] = this.statusCode;
    data['message'] = this.message;
    return data;
  }
}

class Data {
  String ngayChay;
  int idTuyenDuong;
  String tenTuyenDuong;
  int idKhungGio;
  Null thoiGianChay;
  int loaiKhach;
  int soKhach;

  Data(
      {this.ngayChay,
      this.idTuyenDuong,
      this.tenTuyenDuong,
      this.idKhungGio,
      this.thoiGianChay,
      this.loaiKhach,
      this.soKhach});

  Data.fromJson(Map<String, dynamic> json) {
    ngayChay = json['ngayChay'];
    idTuyenDuong = json['idTuyenDuong'];
    tenTuyenDuong = json['tenTuyenDuong'];
    idKhungGio = json['idKhungGio'];
    thoiGianChay = json['thoiGianChay'];
    loaiKhach = json['loaiKhach'];
    soKhach = json['soKhach'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['ngayChay'] = this.ngayChay;
    data['idTuyenDuong'] = this.idTuyenDuong;
    data['tenTuyenDuong'] = this.tenTuyenDuong;
    data['idKhungGio'] = this.idKhungGio;
    data['thoiGianChay'] = this.thoiGianChay;
    data['loaiKhach'] = this.loaiKhach;
    data['soKhach'] = this.soKhach;
    return data;
  }
}
