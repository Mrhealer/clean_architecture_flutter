import 'dart:convert';

import 'package:apis_app/core/utils/constants.dart';
import 'package:apis_app/features/apis/data/models/sign_in_model.dart';
import 'package:flutter/cupertino.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../../../../core/error/exceptions.dart';

abstract class SignInLocalDataSource {
  Future<SignInModel> getLastToken();
  Future<void> cacheToken(SignInModel SignInModel);
}

class SignInLocalDataSourceImpl extends SignInLocalDataSource {
  final SharedPreferences sharedPreferences;

  SignInLocalDataSourceImpl({@required this.sharedPreferences});

  @override
  Future<void> cacheToken(SignInModel SignInModel) {
    return sharedPreferences.setString(CACHED_TOKEN, jsonEncode(SignInModel));
  }

  @override
  Future<SignInModel> getLastToken() {
    String jsonStr = sharedPreferences.getString(CACHED_TOKEN);
    if (jsonStr == null) {
      throw CacheException();
    }
    return Future.value(SignInModel.fromJson(jsonDecode(jsonStr)));
  }
}
