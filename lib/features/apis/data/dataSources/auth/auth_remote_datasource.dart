import 'dart:convert';

import 'package:apis_app/core/network/rest_client_service.dart';
import 'package:apis_app/features/apis/data/models/sign_in_model.dart';
import 'package:apis_app/features/apis/domain/entities/signIn.dart';
import 'package:flutter/cupertino.dart';

import '../../../../../core/error/exceptions.dart';

abstract class SignInRemoteDataSource {
  Future<SignIn> SignInUser(String email, String password);
}

class SignInRemoteDataSourceImpl extends SignInRemoteDataSource {
  final RestClientService restClientService;

  SignInRemoteDataSourceImpl({@required this.restClientService});

  @override
  Future<SignIn> SignInUser(String email, String password) async {
    final response = await restClientService.loginUser(jsonEncode({
      'TenDangNhap': email,
      'MatKhau': password,
    }));
    if (response.statusCode != 200) {
      throw ServerException();
    }
    print(response.body);
    return SignInModel.fromJson(jsonDecode(response.body));
  }
}
