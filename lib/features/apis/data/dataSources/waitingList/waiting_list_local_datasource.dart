import 'package:apis_app/core/error/exceptions.dart';
import 'package:apis_app/core/utils/constants.dart';
import 'package:flutter/cupertino.dart';

import 'package:shared_preferences/shared_preferences.dart';

abstract class WaitingListLocalDataSource {
  Future<bool> clearToken();
}

class WaitingListLocalDataSourceImpl extends WaitingListLocalDataSource {
  final SharedPreferences sharedPreferences;

  WaitingListLocalDataSourceImpl({@required this.sharedPreferences});

  @override
  Future<bool> clearToken() async {
    bool removed = await sharedPreferences.remove(CACHED_TOKEN);
    if (!removed) {
      throw CacheException();
    }
    return removed;
  }
}
