import 'dart:convert';

import 'package:apis_app/core/error/exceptions.dart';
import 'package:apis_app/core/network/rest_client_service.dart';
import 'package:apis_app/features/apis/data/models/waiting_list_model.dart';
import 'package:flutter/cupertino.dart';

abstract class WaitingListRemoteDataSource {
  Future<WaitingListModels> getWaitingList(String strDate);
}

class WaitingListRemoteDataSourceImpl extends WaitingListRemoteDataSource {
  final RestClientService restClientService;

  WaitingListRemoteDataSourceImpl({@required this.restClientService});

  @override
  Future<WaitingListModels> getWaitingList(String strDate) async {
    final response = await restClientService.getWaitingList("");
    print(response);
    if (response.statusCode != 204) {
      throw ServerException();
    }
    return WaitingListModels.fromJson(jsonDecode(response.body));
  }
}
