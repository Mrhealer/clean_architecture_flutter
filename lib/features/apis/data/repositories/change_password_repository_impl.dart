import 'package:apis_app/core/error/exceptions.dart';
import 'package:apis_app/core/error/failures.dart';
import 'package:apis_app/core/network/network_helper.dart';
import 'package:apis_app/features/apis/data/dataSources/changePassword/change_password_local_datasource.dart';
import 'package:apis_app/features/apis/data/dataSources/changePassword/change_password_remote_datasource.dart';
import 'package:apis_app/features/apis/domain/repositories/change_password_repository.dart';
import 'package:dartz/dartz.dart';
import 'package:flutter/cupertino.dart';

class ChangePasswordRepositoryImpl implements ChangePasswordRepository {
  final ChangePasswordRemoteDataSource remoteDataSource;
  final ChangePasswordLocalDataSource localDataSource;
  final NetworkInfo networkInfo;

  ChangePasswordRepositoryImpl(
      {@required this.remoteDataSource,
      @required this.localDataSource,
      @required this.networkInfo});

  @override
  Future<Either<Failure, bool>> changePassword(
      String oldPassword, String newPassword) async {
    if (await networkInfo.isConnected) {
      try {
        final result =
            await remoteDataSource.changePassword(oldPassword, newPassword);
        return Right(result);
      } on ServerException {
        return Left(ServerFailure());
      }
    } else {
      return Left(NoConnectionFailure());
    }
  }
}
