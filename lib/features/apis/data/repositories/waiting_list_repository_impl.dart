import 'package:apis_app/core/error/exceptions.dart';
import 'package:apis_app/core/error/failures.dart';
import 'package:apis_app/core/network/network_helper.dart';
import 'package:apis_app/features/apis/data/dataSources/waitingList/waiting_list_local_datasource.dart';
import 'package:apis_app/features/apis/data/dataSources/waitingList/waiting_list_remote_datasource.dart';
import 'package:apis_app/features/apis/domain/entities/waitingList.dart';
import 'package:apis_app/features/apis/domain/repositories/waiting_list_repository.dart';
import 'package:dartz/dartz.dart';
import 'package:flutter/cupertino.dart';

class WaitingListRepositoryImpl implements WaitingListRepository {
  final WaitingListRemoteDataSource remoteDataSource;
  final WaitingListLocalDataSource localDataSource;
  final NetworkInfo networkInfo;

  WaitingListRepositoryImpl(
      {@required this.remoteDataSource,
      @required this.localDataSource,
      @required this.networkInfo});

  @override
  Future<Either<Failure, WaitingListEntities>> getWaitingList(
      String strDate) async {
    if (await networkInfo.isConnected) {
      try {
        final result = await remoteDataSource.getWaitingList(strDate);
        return Right(result);
      } on ServerException {
        return Left(ServerFailure());
      }
    } else {
      return Left(NoConnectionFailure());
    }
  }
}
