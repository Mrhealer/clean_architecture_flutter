import 'package:apis_app/core/error/exceptions.dart';
import 'package:apis_app/core/error/failures.dart';
import 'package:apis_app/core/network/network_helper.dart';
import 'package:apis_app/features/apis/data/dataSources/home/home_local_datasource.dart';
import 'package:apis_app/features/apis/data/dataSources/home/home_remote_datasource.dart';
import 'package:apis_app/features/apis/domain/repositories/home_repository.dart';
import 'package:dartz/dartz.dart';
import 'package:flutter/cupertino.dart';

class HomeRepositoryImpl implements HomeRepository {
  final HomeRemoteDataSource remoteDataSource;
  final HomeLocalDataSource localDataSource;
  final NetworkInfo networkInfo;

  HomeRepositoryImpl(
      {@required this.remoteDataSource,
        @required this.localDataSource,
        @required this.networkInfo});

  @override
  Future<Either<Failure, bool>> logoutUser(String token) async {
    if (await networkInfo.isConnected) {
      try {
        await remoteDataSource.logoutUser(token);
        try {
          await localDataSource.clearToken();
          return Right(true);
        } on CacheException {
          return Left(CacheFailure());
        }
      } on ServerException {
        return Left(ServerFailure());
      }
    } else {
      return Left(NoConnectionFailure());
    }
  }
}
