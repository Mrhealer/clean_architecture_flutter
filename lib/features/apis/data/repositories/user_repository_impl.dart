import 'package:apis_app/features/apis/data/dataSources/auth/auth_local_datasource.dart';
import 'package:apis_app/features/apis/data/dataSources/auth/auth_remote_datasource.dart';
import 'package:apis_app/features/apis/domain/entities/signIn.dart';
import 'package:apis_app/features/apis/domain/repositories/user_repository.dart';
import 'package:dartz/dartz.dart';
import 'package:flutter/cupertino.dart';

import '../../../../core/error/exceptions.dart';
import '../../../../core/error/failures.dart';
import '../../../../core/network/network_helper.dart';

class UserRepositoryImpl implements UserRepository {
  final SignInRemoteDataSource remoteDataSource;
  final SignInLocalDataSource localDataSource;
  final NetworkInfo networkInfo;

  UserRepositoryImpl(
      {@required this.remoteDataSource,
      @required this.localDataSource,
      @required this.networkInfo});

  @override
  Future<Either<Failure, SignIn>> loginUser(
      String email, String password) async {
    if (await networkInfo.isConnected) {
      try {
        final remoteData = await remoteDataSource.SignInUser(email, password);
        localDataSource.cacheToken(remoteData);
        return Right(remoteData);
      } on ServerException {
        return Left(ServerFailure());
      }
    } else {
      return Left(NoConnectionFailure());
    }
  }

  @override
  Future<Either<Failure, SignIn>> fetchCachedToken() async {
    try {
      final localData = await localDataSource.getLastToken();
      return Right(localData);
    } on CacheException {
      return Left(CacheFailure());
    }
  }
}
