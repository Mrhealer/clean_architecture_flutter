import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

class WaitingListEntities extends Equatable {
  final String token;

  WaitingListEntities({@required this.token}) : super([token]);

  @override
  // TODO: implement props
  List<Object> get props => throw UnimplementedError();
}
