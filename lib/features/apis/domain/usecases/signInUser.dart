
import 'package:apis_app/features/apis/domain/entities/signIn.dart';
import 'package:apis_app/features/apis/domain/repositories/user_repository.dart';
import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

import '../../../../core/error/failures.dart';
import '../../../../core/usecases/usercase.dart';

class SignInUser implements UseCase<SignIn, LoginParams> {
  final UserRepository repository;

  SignInUser({@required this.repository});

  @override
  Future<Either<Failure, SignIn>> call(LoginParams params) async {
    return await repository.loginUser(params.email, params.password);
  }
}

class LoginParams extends Equatable {
  final String email;
  final String password;

  LoginParams({@required this.email, @required this.password})
      : super([email, password]);
}
