import 'package:apis_app/core/error/failures.dart';
import 'package:apis_app/core/usecases/usercase.dart';
import 'package:apis_app/features/apis/domain/repositories/home_repository.dart';
import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';

class LogOutUser implements UseCase<bool, LogOutParams> {
  final HomeRepository repository;

  LogOutUser({@required this.repository});

  @override
  Future<Either<Failure, bool>> call(LogOutParams params) async {
    return await repository.logoutUser(params.token);
  }
}

class LogOutParams extends Equatable {
  final String token;
  LogOutParams({@required this.token}) : super([token]);
}
