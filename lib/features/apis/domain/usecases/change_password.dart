import 'package:apis_app/core/error/failures.dart';
import 'package:apis_app/core/usecases/usercase.dart';
import 'package:apis_app/features/apis/domain/repositories/change_password_repository.dart';
import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';

class ChangePassword extends UseCase<bool,ChangePasswordParams>{
  final ChangePasswordRepository repository;

  ChangePassword({this.repository});

  @override
  Future<Either<Failure, bool>> call(ChangePasswordParams params) {
    return repository.changePassword(params.oldPassword, params.newPassword);
  }
}

class ChangePasswordParams extends Equatable{
  final String oldPassword;
  final String newPassword;

  ChangePasswordParams({@required this.oldPassword, @required this.newPassword});
}