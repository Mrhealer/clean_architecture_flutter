import 'package:apis_app/core/error/failures.dart';
import 'package:apis_app/core/usecases/usercase.dart';
import 'package:apis_app/features/apis/domain/entities/waitingList.dart';
import 'package:apis_app/features/apis/domain/repositories/waiting_list_repository.dart';
import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';

class WaitingList extends UseCase<WaitingListEntities, WaitingListParams> {
  final WaitingListRepository repository;

  WaitingList({this.repository});

  @override
  Future<Either<Failure, WaitingListEntities>> call(WaitingListParams params) {
    return repository.getWaitingList(params.strDate);
  }
}

class WaitingListParams extends Equatable {
  final String strDate;

  WaitingListParams({@required this.strDate});
}
