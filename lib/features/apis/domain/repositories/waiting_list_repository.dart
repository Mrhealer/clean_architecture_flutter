import 'package:apis_app/features/apis/domain/entities/waitingList.dart';
import 'package:dartz/dartz.dart';

import '../../../../core/error/failures.dart';

abstract class WaitingListRepository {
  Future<Either<Failure, WaitingListEntities>> getWaitingList(String strDate);
}
