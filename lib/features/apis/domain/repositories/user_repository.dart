import 'package:apis_app/features/apis/domain/entities/signIn.dart';
import 'package:dartz/dartz.dart';

import '../../../../core/error/exceptions.dart';
import '../../../../core/error/failures.dart';

abstract class UserRepository {
  Future<Either<Failure, SignIn>> loginUser(String email, String password);
  Future<Either<Failure, SignIn>> fetchCachedToken();
}
