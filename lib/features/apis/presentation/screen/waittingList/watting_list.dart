import 'package:apis_app/features/apis/presentation/screen/waittingList/components/body.dart';
import 'package:flutter/material.dart';

class WaitingListScreen extends StatefulWidget {
  const WaitingListScreen({Key key}) : super(key: key);

  @override
  _WaitingListScreenState createState() => _WaitingListScreenState();
}

class _WaitingListScreenState extends State<WaitingListScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          title: Text("Danh Sach"), backgroundColor: Colors.lightBlue[900]),
      body: Container(
        child: Body(),
      ),
    );
  }
}
