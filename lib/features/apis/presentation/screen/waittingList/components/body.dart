import 'package:apis_app/core/utils/constants.dart';
import 'package:apis_app/features/apis/bloc/waitingList/waiting_list_bloc.dart';
import 'package:apis_app/features/apis/bloc/waitingList/waiting_list_event.dart';
import 'package:apis_app/features/apis/bloc/waitingList/waiting_list_state.dart';
import 'package:apis_app/features/apis/presentation/widgets/extentions/theme.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../../../../injector.dart';

class Body extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return _buildBody(context);
  }

  BlocProvider<WaitingListBloc> _buildBody(BuildContext context) {
    final Size size = MediaQuery.of(context).size;
    return BlocProvider<WaitingListBloc>(
      create: (_) => sl<WaitingListBloc>(),
      child: Container(
        height: size.height,
        width: size.width,
        padding: EdgeInsets.all(DEFAULT_PAGE_PADDING),
        child: Column(
          children: <Widget>[
            Padding(padding: EdgeInsets.only(top: 50)),
            Text(
              "Draft Home",
              style: CustomTheme.mainTheme.textTheme.title,
            ),
            Padding(padding: EdgeInsets.only(top: 50)),
            Column(
              mainAxisAlignment: MainAxisAlignment.end,
              mainAxisSize: MainAxisSize.max,
              children: <Widget>[
                Container(
                  width: double.infinity,
                  height: 36,
                  child: _buildWaitingList(context),
                ),
                Padding(
                  padding: EdgeInsets.only(top: 8),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }

  BlocBuilder _buildWaitingList(BuildContext context) {
    return BlocBuilder<WaitingListBloc, WaitingListState>(

      builder: (context, state) {
        return RaisedButton(
          shape: RoundedRectangleBorder(
            borderRadius: new BorderRadius.circular(4.0),
          ),
          color: CustomColor.logoBlue,
          onPressed: () {},
          child: Text(
            "SIGN OUT",
            style: CustomTheme.mainTheme.textTheme.button,
          ),
        );
      },
    );
  }
}
