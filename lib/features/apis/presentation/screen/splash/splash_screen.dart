// import 'dart:async';
//
// import 'package:flutter/material.dart';
//
// class SplashScreen extends StatefulWidget {
//   const SplashScreen({Key key}) : super(key: key);
//
//   @override
//   State<StatefulWidget> createState() => _SplashScreenState();
// }
//
// class _SplashScreenState extends State<SplashScreen>
//     with SingleTickerProviderStateMixin {
//   var _visible = true;
//
//    AnimationController animationController;Animation<double> animation;
//
//   startTime() async {
//     var _duration = const Duration(seconds: 3);
//     return Timer(_duration, navigationPage);
//   }
//
//   void navigationPage() async {}
//
//   @override
//   void initState() {
//     super.initState();
//
//     animationController =
//         AnimationController(vsync: this, duration: const Duration(seconds: 2));
//     animation =
//         CurvedAnimation(parent: animationController, curve: Curves.easeOut);
//
//     animation.addListener(() => setState(() {}));
//     animationController.forward();
//
//     if (!mounted) return;
//     setState(() {
//       _visible = !_visible;
//     });
//     startTime();
//   }
//
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       body: Stack(
//         fit: StackFit.expand,
//         children: <Widget>[
//           Column(
//             mainAxisAlignment: MainAxisAlignment.end,
//             mainAxisSize: MainAxisSize.min,
//             children: <Widget>[
//               CircularProgressIndicator(),
//               SizedBox(height: 10),
//               Text(
//                 "Loading",
//                 style: TextStyle(
//                   fontSize: 32,
//                   fontWeight: FontWeight.bold,
//                 ),
//               ),
//               SizedBox(height: 20),
//             ],
//           ),
//           Column(
//             mainAxisAlignment: MainAxisAlignment.center,
//             children: <Widget>[
//               Image.asset(
//                 'assets/logo/logo.jpg',
//                 width: animation.value * 250,
//                 height: animation.value * 250,
//               ),
//             ],
//           ),
//         ],
//       ),
//     );
//   }
// }
