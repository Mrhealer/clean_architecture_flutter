import 'package:apis_app/core/utils/constants.dart';
import 'package:apis_app/features/apis/presentation/screen/changePassword/change_password.dart';
import 'package:apis_app/features/apis/presentation/screen/home/home_screen.dart';
import 'package:apis_app/features/apis/presentation/screen/signIn/signInScreen.dart';
import 'package:flutter/material.dart';

import 'empty.dart';

class RouterGenerator {
  static Route<dynamic> generateRouter(RouteSettings settings) {
    final args = settings.arguments;

    switch (settings.name) {
      case HOME_ROUTE:
        return MaterialPageRoute(builder: (_) => HomePage());
      case LOGIN_ROUTE:
        return MaterialPageRoute(builder: (_) => SignInPage());
      case CHANGE_PASSWORD:
        return MaterialPageRoute(builder: (_) => ChangePasswordPage());

      default:
        {
          return MaterialPageRoute(builder: (_) => WidgetNotFound());
        }
    }
  }
}
