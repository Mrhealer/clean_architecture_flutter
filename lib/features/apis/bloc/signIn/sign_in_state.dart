import 'package:apis_app/features/apis/domain/entities/signIn.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class SignInState extends Equatable {
  SignInState([List props = const <dynamic>[]]) : super(props);
}

class NotLoggedState extends SignInState {}

class LoadingState extends SignInState {}

class LoggedState extends SignInState {
  final SignIn login;

  LoggedState({@required this.login}) : super([login]);
}

class ErrorState extends SignInState {
  final String message;

  ErrorState({@required this.message}) : super([message]);
}
