import 'dart:async';

import 'package:apis_app/core/usecases/fetch_token.dart';
import 'package:apis_app/core/utils/constants.dart';
import 'package:apis_app/features/apis/bloc/signIn/sign_in_event.dart';
import 'package:apis_app/features/apis/bloc/signIn/sign_in_state.dart';
import 'package:apis_app/features/apis/domain/entities/signIn.dart';
import 'package:apis_app/features/apis/domain/usecases/signInUser.dart';
import 'package:bloc/bloc.dart';
import 'package:flutter/cupertino.dart';

import '../../../../core/error/failures.dart';

class SignInBloc extends Bloc<SignInEvent, SignInState> {
  final SignInUser loginUser;
  final FetchToken fetchToken;

  SignInBloc({@required this.loginUser, @required this.fetchToken})
      : super(null);

  @override
  SignInState get initialState => NotLoggedState();

  @override
  Stream<SignInState> mapEventToState(SignInEvent event) async* {
    if (event is CheckSignInStatusEvent) {
      yield LoadingState();
      final result = await fetchToken(TokenParams());
      yield* result.fold((failure) async* {
        yield NotLoggedState();
      }, (success) async* {
        yield LoggedState(login: SignIn(token: success.token));
      });
    } else if (event is LoginEvent) {
      yield LoadingState();
      final result = await loginUser(
          LoginParams(email: event.email, password: event.password));
      yield* result.fold((failure) async* {
        if (failure is NoConnectionFailure) {
          yield ErrorState(message: NO_CONNECTION_ERROR);
        } else {
          yield ErrorState(message: LOGGING_ERROR);
        }
      }, (success) async* {
        yield LoggedState(login: SignIn(token: success.token));
      });
    } else if (event is SkipLoginEvent) {
      yield LoggedState(login: SignIn(token: "111111111111111111111111"));
    }
  }
}
