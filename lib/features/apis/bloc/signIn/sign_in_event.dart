import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class SignInEvent extends Equatable {
  SignInEvent([List props = const <dynamic>[]]) : super(props);
}

class CheckSignInStatusEvent extends SignInEvent {}

class LoginEvent extends SignInEvent {
  final String email;
  final String password;

  LoginEvent(this.email, this.password);
}

class SkipLoginEvent extends SignInEvent {}
