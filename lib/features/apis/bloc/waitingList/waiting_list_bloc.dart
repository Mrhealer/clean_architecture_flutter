import 'dart:async';

import 'package:apis_app/core/utils/constants.dart';
import 'package:apis_app/features/apis/bloc/waitingList/waiting_list_event.dart';
import 'package:apis_app/features/apis/bloc/waitingList/waiting_list_state.dart';
import 'package:apis_app/features/apis/domain/usecases/waiting_list.dart';
import 'package:bloc/bloc.dart';
import 'package:flutter/cupertino.dart';

class WaitingListBloc extends Bloc<WaitingListEvent, WaitingListState> {
  final WaitingList waitingList;

  WaitingListBloc({@required this.waitingList}) : super(null);

  @override
  WaitingListState get initialState => GetDataState();

  @override
  Stream<WaitingListState> mapEventToState(WaitingListEvent event) async* {
    if (event is CheckWaitingListStatusEvent) {
      yield LoadingState();
      final result =
          await waitingList(WaitingListParams(strDate: event.strDate));
      yield* result.fold((failure) async* {
        yield ErrorState(message: GET_WAITING_LIST_ERROR);
      }, (success) async* {
        yield SuccessfulState();
      });
    }
  }
}
