import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class WaitingListEvent extends Equatable {
  WaitingListEvent([List props = const <dynamic>[]]) : super(props);
}

class CheckWaitingListStatusEvent extends WaitingListEvent {
  final String strDate;

  CheckWaitingListStatusEvent({@required this.strDate});
}

class SkipWaitingListEvent extends WaitingListEvent {}
