import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class WaitingListState extends Equatable {
  WaitingListState([List props = const <dynamic>[]]) : super(props);
}

class GetDataState extends WaitingListState {}

class LoadingState extends WaitingListState {}

class SuccessfulState extends WaitingListState {}

class ErrorState extends WaitingListState {
  final String message;

  ErrorState( {@required this.message}) : super([message]);
}
