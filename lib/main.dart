import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'core/utils/constants.dart';
import 'features/apis/presentation/routers/router_generator.dart';
import 'features/apis/presentation/widgets/extentions/colors.dart';
import 'injector.dart' as di;

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await di.init();
  SystemChrome.setSystemUIOverlayStyle(
      const SystemUiOverlayStyle(statusBarColor: Colors.white));
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "Apis",
      theme: ThemeData(
        primarySwatch: Colors.amber,
        primaryColor: Colours.color_app,
        backgroundColor: Colours.color_app,
        fontFamily: "OpenSans",
        primaryTextTheme:
            const TextTheme(headline6: TextStyle(color: Colors.white)),
        iconTheme: const IconThemeData(color: Colors.white),
      ),
      initialRoute: LOGIN_ROUTE,
      onGenerateRoute: RouterGenerator.generateRouter,
      debugShowCheckedModeBanner: false,
    );
  }
}
