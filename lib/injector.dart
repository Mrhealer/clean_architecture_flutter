import 'package:apis_app/features/apis/bloc/signIn/sign_in_bloc.dart';
import 'package:apis_app/features/apis/bloc/waitingList/waiting_list_bloc.dart';
import 'package:apis_app/features/apis/data/dataSources/auth/auth_local_datasource.dart';
import 'package:apis_app/features/apis/data/dataSources/auth/auth_remote_datasource.dart';
import 'package:apis_app/features/apis/data/dataSources/waitingList/waiting_list_local_datasource.dart';
import 'package:apis_app/features/apis/data/dataSources/waitingList/waiting_list_remote_datasource.dart';
import 'package:apis_app/features/apis/data/repositories/user_repository_impl.dart';
import 'package:apis_app/features/apis/data/repositories/waiting_list_repository_impl.dart';
import 'package:apis_app/features/apis/domain/repositories/user_repository.dart';
import 'package:apis_app/features/apis/domain/usecases/signInUser.dart';
import 'package:apis_app/features/apis/domain/usecases/waiting_list.dart';
import 'package:chopper/chopper.dart';
import 'package:data_connection_checker/data_connection_checker.dart';
import 'package:get_it/get_it.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'core/network/network_helper.dart';
import 'core/network/rest_client_service.dart';
import 'core/usecases/fetch_token.dart';
import 'features/apis/bloc/changePassword/change_password_bloc.dart';
import 'features/apis/bloc/logOut/log_out_bloc.dart';
import 'features/apis/bloc/signIn/sign_in_event.dart';
import 'features/apis/data/dataSources/changePassword/change_password_local_datasource.dart';
import 'features/apis/data/dataSources/changePassword/change_password_remote_datasource.dart';
import 'features/apis/data/dataSources/home/home_local_datasource.dart';
import 'features/apis/data/dataSources/home/home_remote_datasource.dart';
import 'features/apis/data/repositories/change_password_repository_impl.dart';
import 'features/apis/data/repositories/home_repository_impl.dart';
import 'features/apis/domain/repositories/change_password_repository.dart';
import 'features/apis/domain/repositories/home_repository.dart';
import 'features/apis/domain/repositories/waiting_list_repository.dart';
import 'features/apis/domain/usecases/change_password.dart';
import 'features/apis/domain/usecases/logoutUser.dart';

final sl = GetIt.instance; //sl is referred to as Service Locator

//Dependency injection
Future<void> init() async {
  //Blocs
  sl.registerFactory(
    () => SignInBloc(
      loginUser: sl(),
      fetchToken: sl(),
    )..add(CheckSignInStatusEvent()),
  );
  sl.registerFactory(
    () => LogOutBloc(
      fetchToken: sl(),
      logoutUser: sl(),
    ),
  );
  sl.registerFactory(
    () => ChangePasswordBloc(
      changePassword: sl(),
    ),
  );
  sl.registerFactory(
    () => WaitingListBloc(
      waitingList: sl(),
    ),
  );

  //Use cases
  sl.registerLazySingleton(() => SignInUser(repository: sl()));
  sl.registerLazySingleton(() => FetchToken(repository: sl()));
  sl.registerLazySingleton(() => LogOutUser(repository: sl()));
  sl.registerLazySingleton(() => ChangePassword(repository: sl()));
  sl.registerLazySingleton(() => WaitingList(repository: sl()));

  //Repositories
  sl.registerLazySingleton<UserRepository>(
    () => UserRepositoryImpl(
      networkInfo: sl(),
      localDataSource: sl(),
      remoteDataSource: sl(),
    ),
  );
  sl.registerLazySingleton<HomeRepository>(() => HomeRepositoryImpl(
        networkInfo: sl(),
        localDataSource: sl(),
        remoteDataSource: sl(),
      ));
  sl.registerLazySingleton<ChangePasswordRepository>(
      () => ChangePasswordRepositoryImpl(
            networkInfo: sl(),
            localDataSource: sl(),
            remoteDataSource: sl(),
          ));

  sl.registerLazySingleton<WaitingListRepository>(
      () => WaitingListRepositoryImpl(
            networkInfo: sl(),
            localDataSource: sl(),
            remoteDataSource: sl(),
          ));

  //Data sources
  sl.registerLazySingleton<SignInRemoteDataSource>(
    () => SignInRemoteDataSourceImpl(
      restClientService: sl(),
    ),
  );
  sl.registerLazySingleton<SignInLocalDataSource>(
    () => SignInLocalDataSourceImpl(
      sharedPreferences: sl(),
    ),
  );
  sl.registerLazySingleton<HomeRemoteDataSource>(
    () => HomeRemoteDataSourceImpl(
      restClientService: sl(),
    ),
  );
  sl.registerLazySingleton<HomeLocalDataSource>(
    () => HomeLocalDataSourceImpl(
      sharedPreferences: sl(),
    ),
  );
  sl.registerLazySingleton<ChangePasswordRemoteDataSource>(
    () => ChangePasswordRemoteDataSourceImpl(
      restClientService: sl(),
    ),
  );
  sl.registerLazySingleton<ChangePasswordLocalDataSource>(
    () => ChangePasswordLocalDataSourceImpl(
      sharedPreferences: sl(),
    ),
  );
  sl.registerLazySingleton<WaitingListLocalDataSource>(
    () => WaitingListLocalDataSourceImpl(
      sharedPreferences: sl(),
    ),
  );

  sl.registerLazySingleton<WaitingListRemoteDataSource>(
    () => WaitingListRemoteDataSourceImpl(
      restClientService: sl(),
    ),
  );

  //Core
  sl.registerLazySingleton<NetworkInfo>(
    () => NetworkInfoImpl(dataConnectionChecker: sl()),
  );

  //External
  final SharedPreferences sharedPreferences =
      await SharedPreferences.getInstance();
  sl.registerLazySingleton(() => sharedPreferences);
  sl.registerLazySingleton(() => DataConnectionChecker());
  final client = ChopperClient(interceptors: [
    CurlInterceptor(),
    HttpLoggingInterceptor(),
  ]);
  sl.registerLazySingleton(() => RestClientService.create(client));
}
