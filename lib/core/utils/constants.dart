const String CACHED_TOKEN = 'CACHED_TOKEN';

const String API_BASE_URL = 'https://apidev.trungchuyenhn.com/api/v1/'; // add the base url here
const String LOGIN_USER = 'taikhoan/dangnhap';
const String GET_WAITING_LIST = "trungchuyen";
const String DETAIL_CUSTOMER= "";
const String UPDATE_STATUS = "capnhat-trangthai";
const String UPDATE_DEVICE_TOKEN = "taikhoan/capnhat-device-token";
const String UPDATE_DRIVER = "taikhoan/capnhat-trangthai-online";
const String CHANGE_PASSWORD = "taikhoan/capnhat-matkhau";

const String CREATE_USER = 'create';

const String LOGGING_ERROR =
    'Could not login successfully, please check your email and password';
const String LOGGING_OUT_ERROR =
    'Could not logout successfully, please try again later';
const String NO_CONNECTION_ERROR = 'You are not connected to the Internet';
const String CHANGE_PASSWORD_ERROR =
    'Could not change the password. Please try again later';

const String GET_WAITING_LIST_ERROR = 'Could not get waiting list. Please try again later';

const double DEFAULT_PAGE_PADDING = 20;

//routes
const String HOME_ROUTE = '/';
const String LOGIN_ROUTE = '/login';
const String CHANGE_PASSWORD_ROUTE = '/change_password';
