import 'package:apis_app/features/apis/domain/entities/signIn.dart';
import 'package:apis_app/features/apis/domain/repositories/user_repository.dart';
import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';

import '../error/failures.dart';
import 'usercase.dart';

class FetchToken implements UseCase<SignIn, TokenParams> {
  final UserRepository repository;

  FetchToken({@required this.repository});

  @override
  Future<Either<Failure, SignIn>> call(TokenParams params) async {
    return await repository.fetchCachedToken();
  }
}

class TokenParams extends Equatable {
  @override
  // TODO: implement props
  List<Object> get props => throw UnimplementedError();
}
