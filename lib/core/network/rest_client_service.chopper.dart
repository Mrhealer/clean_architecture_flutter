// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'rest_client_service.dart';

// **************************************************************************
// ChopperGenerator
// **************************************************************************

class _$RestClientService extends RestClientService {
  _$RestClientService([ChopperClient client]) {
    if (client == null) return;
    this.client = client;
  }

  final definitionType = RestClientService;

  Future<Response> loginUser(String jsonBody) {
    final $url = API_BASE_URL + LOGIN_USER;
    final $headers = {'Content-type': 'application/json'};
    final $body = jsonBody;
    final $request =
        Request('POST', $url, client.baseUrl, body: $body, headers: $headers);
    return client.send<dynamic, dynamic>($request);
  }

  Future<Response> logoutUser(String jsonBody, String token) {
    final $url = 'BASE_URL/tokens';
    final $headers = {
      'Authorization': token,
      'Content-type': 'application/json'
    };
    final $body = jsonBody;
    final $request =
        Request('DELETE', $url, client.baseUrl, body: $body, headers: $headers);
    return client.send<dynamic, dynamic>($request);
  }

  Future<Response> changePassword(String jsonBody) {
    final $url = 'BASE_URL/create';
    final $headers = {'Content-type': 'application/json'};
    final $body = jsonBody;
    final $request =
        Request('PUT', $url, client.baseUrl, body: $body, headers: $headers);
    return client.send<dynamic, dynamic>($request);
  }

  @override
  Future<Response> getWaitingList(String strDate) {

    final $url = API_BASE_URL + GET_WAITING_LIST + "2021-06-15";
    final $headers = {
      'Authorization': "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1bmlxdWVfbmFtZSI6IjA5MTIzNDU2NzgiLCJuYW1laWQiOiI0NTgxN2FkZC0yYTBhLTRkODctYjE5OC01ZmZmOTNlYWY5YWMiLCJyb2xlIjoiMyIsIm5oYXhlIjoiMiIsImlhdCI6MTYyMzgxMzkxNiwibW9iaWxlX3Bob25lIjoiMDkxMjM0NTY3OCIsIm5iZiI6MTYyMzgxMzkxNiwiZXhwIjoxNjIzODU3MTE2LCJpc3MiOiJ5b3VyZG9tYWluLmNvbSIsImF1ZCI6InlvdXJkb21haW4uY29tIn0.SdJG7Sh0uxo1qMIdM4QKCWIITRFrFZY2VcRn5V9zgak",
      'Content-type': 'application/json'
    };
    final $request = Request('GET', $url, client.baseUrl, headers: $headers);
    return client.send<dynamic, dynamic>($request);
  }
}
